var http = require('http');
var express = require('express')
var cors = require('cors')
var axios = require("axios")

var players = []
var fights = []

const port = 8080;

//Server start
var app = express()
app.use(cors())
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// On client connection
io.sockets.on('connection', function (socket) {
	console.log('Client connected');
	
	socket.on('join', function (message) {

		console.log(`${message.login} wants to fight!`);

		if(players.length > 0){
			var user1 = players.pop()
			var player = {
				id: message.id,
				login: message.login,
				socketId: socket.id
			}
			if(user1.id === player.id){
				players.push(player)
			}
			else{
				var fight = {
					user1: user1,
					user2: player,
					stageUser1: 1,
					stageUser2: 1,
				}
				fights.push(fight)
				io.emit('fight', {user1: user1, user2: player});
				console.log(`Let's the show start!!! ${user1.login} VS ${player.login}`);
			}
		}
		else if(players.findIndex(player => player.id === message.id) === -1){
			var player = {
				id: message.id,
				login: message.login,
				socketId: socket.id
			}
			players.push(player)
		}
	});

	socket.on('message', function (message) {
       	socket.broadcast.emit('message', message);
	});

	socket.on('cardChosen', function (message) {
		socket.broadcast.emit('cardChosen', message);
		var fight = fights.find(fight => fight.user1 === message.id || fight.user2 === message.id)
		if(fight.user1 === message.id){
			fight.stageUser1 = 2
		}
		else if(fight.user2 === message.id){
			fight.stageUser2 = 2
		}

		if(fight.stageUser1 === 2 && fight.stageUser2 === 2){
			io.emit("fight2", {fight, firstPlayer: Math.random() < 0.5 ? 1 : 2})
		}
	});
	
	socket.on('attack', function (message) {
		socket.broadcast.emit('attack', message);
	});
	
	socket.on('leave', function (message) {
		console.log(`${message.login} left the game`);
       	socket.broadcast.emit('leave', socket.id);
	});

	socket.on('disconnect', function () {
		console.log("Client disconnected")
		socket.broadcast.emit('leave', socket.id);
	})

	socket.on('error', function (err) {
		console.log('Client error', socket.id);
		console.log(err);
	})

});

// Display registered users
server.listen(port, function (err){
	if (err) throw err;
	console.log(`Socket listening on port ${port}`);
})

async function getUsers() {
    try {
        const response = await axios({
            method: "get",
            url: "http://localhost:9000/users",
            responseType: "json",
        });
        return response;
    } catch (error) {
        return error;
    }
}

getUsers().then(response => {
	if(response.data){
		console.log("Registered users: " + response.data.map(user => user.login))
	}
})